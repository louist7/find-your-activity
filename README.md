# Java / AngularJS project built using Maven

# Introduction

Find your activity web application

# Pre-requisites

* Firefox or Chrome
* npm
* bower
* JDK
* maven

# Installation

```from command line
cd {PROJECT_HOME_DIR}

mvn package

When running this command:
* Bower install will be run
* Standard maven phases
* War file generated in target directory

# View application in browser

Start up your server and deploy generated war file.
http://localhost:8080/find-your-activity/#/


# Version control

```password protected git repo held on Bitbucket
git clone https://louist7@bitbucket.org/louist7/java-angularjs.git

