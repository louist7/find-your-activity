package fya.service;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import fya.hibernate.User;
import fya.hibernate.UserDaoHibernate;
import fya.service.ActivityServiceImpl;

public class ActivityServiceImplTest {

	private User user;
	private UserDaoHibernate userDao = new UserDaoHibernate();
	private ActivityServiceImpl activityService = new ActivityServiceImpl();
	
	private static Long USER_ID = 1L;

    @Before
    public void setUp() throws Exception {
    	user = userDao.retrieve(USER_ID);
		assertNotNull(user);
		
    }

    @Test
    public void testCalculateActivityResults() {
    	assertNotNull(activityService.calculateActivityResults(user));
    }
    
    @Test
    public void testCalculateBestRatedActivitiesForUsers() {
    	
    	List<Long> userIDs = new ArrayList<Long>();
    	userIDs.add(USER_ID);
    	
    	assertNotNull(activityService.calculateBestRatedActivitiesForUsers(userIDs));
    }
    
    @Test
    public void testCalculateAllBestRatedActivities() {
    	assertNotNull(activityService.calculateAllBestRatedActivities());
    }
    
    @Test
    public void testCalculateNearestActivities() {
    	assertNotNull(activityService.calculateNearestActivities(user.getPostcode()));
    }
}
