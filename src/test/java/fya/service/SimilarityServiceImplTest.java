package fya.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import fya.hibernate.Activity;
import fya.hibernate.ActivityDaoHibernate;
import fya.hibernate.User;
import fya.hibernate.UserDaoHibernate;

public class SimilarityServiceImplTest {

	private User user;
	private UserDaoHibernate userDao = new UserDaoHibernate();
	private Activity activity;
	private ActivityDaoHibernate activityDao = new ActivityDaoHibernate();
	
	private SimilarityServiceImpl similarityService = new SimilarityServiceImpl();
	
	private static Long ID = 1L;

    @Before
    public void setUp() throws Exception {
    	user = userDao.retrieve(ID);
    	activity = activityDao.getById(ID);
    	
		assertNotNull(user);
		assertNotNull(activity);
    }
	
	@Test
	public void testPearsonCorrelation() {
		
		int[] array1 = similarityService.userToIntArray(user);
		int[] array2 = similarityService.activityToIntArray(activity);
		
		assertNotNull(similarityService.calculatePearsonCorrelation(array1, array2));
	}

}
