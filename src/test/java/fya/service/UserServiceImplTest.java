package fya.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import fya.hibernate.User;
import fya.hibernate.UserDaoHibernate;

public class UserServiceImplTest {

	private User user;
	private UserDaoHibernate userDao = new UserDaoHibernate();
	private UserServiceImpl userService = new UserServiceImpl();
	
	private static Long USER_ID = 1L;
	
    @Before
    public void setUp() throws Exception {
    	user = userDao.retrieve(USER_ID);
		assertNotNull(user);
    }

    @Test
    public void testCalculateSimilarUsers() {
    	assertNotNull(userService.calculateSimilarUsers(user, 50, false));
    }
}
