package fya.rest;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import fya.rest.ActivityApplication;

public class ActivityApplicationTest {

    private ActivityApplication activityApplication;

    @Before
    public void setUp() throws Exception {
        this.activityApplication = new ActivityApplication();
    }

    @Test
    public void testGetClasses() throws Exception {
        final Set<Class<?>> classes = this.activityApplication.getClasses();
        assertNotNull(classes);
    }
}