package fya.hibernate;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class UserDaoHibernateTest {

	private User user;
	private UserDaoHibernate userDao = new UserDaoHibernate();
	
	private static Long ID = 1L;
	
	@Before
    public void setUp() throws Exception {
	
		user = userDao.retrieve(ID);
		assertNotNull(user);
	}

	@Test
	public void testCheckLogin() {
		assertNotNull(userDao.checkLogin(user.getUsername(), user.getPassword()));
	}
	
	@Test
	public void testCheckUsernameExists() {
		assertNotNull(userDao.checkUsernameExists(user.getUsername()));
	}
	
	@Test
	public void testRetrieveByUserName() {
		assertNotNull(userDao.retrieveByUserName(user.getUsername()));
	}
	
	@Test
	public void testGetAllUsers() {
		assertNotNull(userDao.getAllUsers());
	}
}
