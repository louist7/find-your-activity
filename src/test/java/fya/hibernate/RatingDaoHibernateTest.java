package fya.hibernate;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class RatingDaoHibernateTest {

	private Rating rating;
	private RatingDaoHibernate ratingDao = new RatingDaoHibernate();
	
	private static Long ID = 1L;
	
	@Before
    public void setUp() throws Exception {
	
		rating = ratingDao.getById(ID);
		assertNotNull(rating);
	}
	
	@Test
	public void testRetrieve() {
		assertNotNull(ratingDao.retrieve(rating.getUserId(), rating.getActivityId()));
	}

	@Test
	public void testRetrieveRatingsForActivity() {
		assertNotNull(ratingDao.retrieveRatingsForActivity(rating.getActivityId()));
	}
	
	@Test
	public void testRetrieveRatingsForActivityAndUsers() {
	
		List<Long> userIds = new ArrayList<Long>();
		userIds.add(rating.getUserId());
		
		assertNotNull(ratingDao.retrieveRatingsForActivityAndUsers(rating.getActivityId(), userIds));
	}
}
