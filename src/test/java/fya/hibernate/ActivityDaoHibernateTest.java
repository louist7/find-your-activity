package fya.hibernate;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ActivityDaoHibernateTest {
	
	private Activity activity;
	private ActivityDaoHibernate activityDao = new ActivityDaoHibernate();
	
	private static Long ID = 1L;
	
	@Before
    public void setUp() throws Exception {
	
		activity = activityDao.getById(ID);
		assertNotNull(activity);
	}

	@Test
	public void testRetrieveByName() {
		assertNotNull(activityDao.retrieveByName(activity.getName()));
	}
	
	@Test
	public void testGetAllActivities() {
		assertNotNull(activityDao.getAllActivities());
	}

}
