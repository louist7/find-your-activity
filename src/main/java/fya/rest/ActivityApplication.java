package fya.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import java.util.HashSet;
import java.util.Set;

@ApplicationPath("resource")
public class ActivityApplication extends Application {

	private Set<Class<?>> resources = new HashSet<>();
	public static final String HOME_RESOURCE_ROOT = "/home";
	public static final String SEARCH_RESOURCE_ROOT = "/search";
	public static final String USER_RESOURCE_ROOT = "/user";
	public static final String ADMIN_RESOURCE_ROOT = "/admin";
	
    @Override
    public Set<Class<?>> getClasses() {
        populateResources(resources);
        return resources;
    }
    
    private void populateResources(Set<Class<?>> resources) {
    	resources.add(HomeResource.class);
    	resources.add(SearchResource.class);
    	resources.add(UserResource.class);
    	resources.add(AdminResource.class);
    }
}
