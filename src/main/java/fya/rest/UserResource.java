package fya.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;

import com.fasterxml.jackson.core.JsonProcessingException;

import fya.hibernate.Rating;
import fya.hibernate.User;
import fya.service.UserService;

@Path(ActivityApplication.USER_RESOURCE_ROOT)
@RequestScoped
public class UserResource {

	@Inject
	private UserService userService;
	private static final String USERNAME = "username";
	private static final String PASSWORD = "password";
	
    @POST
    @Path("/login")
    public Response login(@RequestBody JsonObject loginForm, @Context HttpServletRequest request) throws JsonProcessingException {
        
    	User user = getUserService().login(loginForm.getString(USERNAME), loginForm.getString(PASSWORD));
        
    	if (user != null) {
        	return Response.ok(user).build();
    	} else {
    		return Response.noContent().build();
    	}
    }
    
    @POST
    @Path("/signup")
    public Response signUpNewUser(@RequestBody JsonObject signupForm, @Context HttpServletRequest request) {
    	
    	User user = getUserService().createUser(signupForm.getString(USERNAME), signupForm.getString(PASSWORD));
    	
    	if (user != null) {
    		return Response.ok(user).build();
    	} else {
    		return Response.noContent().build();
    	}
    }
    
    @POST
    @Path("/findSimilarUsers/{userDistance}")
    public Response findSimilarUsers(@PathParam("userDistance") int userDistance, @RequestBody User user) {

    	List<JSONObject> results = getUserService().calculateSimilarUsers(user, userDistance, true);
    	
    	if (results == null) {
    		return Response.noContent().build();
    	}
    	
    	return Response.ok(results.toString()).build();
    }
    
    @POST
    @Path("/updateUserProfile/{username}")
    public Response updateUserProfile(@PathParam("username") String username, @RequestBody JsonObject details) {

    	String email = details.getString("email");
    	boolean visible = details.getBoolean("visibleProfile");
    	String phone = details.getString("phone");
    	
    	User user = getUserService().retrieveByUsername(username);
    	
    	user.setEmail(email);
    	user.setVisibleProfile(visible);
    	user.setPhone(phone);
    	
    	getUserService().updateUserDetails(user);
    	
    	return Response.ok(user).build();
    }

    @POST
    @Path("/saveActivityRating/{userId}/{activityId}/{rating}")
    public Response saveActivityRating(@PathParam("userId") Long userId, @PathParam("activityId") Long activityId, @PathParam("rating") int rating) {

    	Rating userRating = getUserService().retrieveRating(userId, activityId);
    	
    	if (userRating == null) {
    		userRating = new Rating();
    		userRating.setUserId(userId);
        	userRating.setActivityId(activityId);
        	userRating.setRating(rating);
        	
        	getUserService().storeRating(userRating);
    	} else {
    		userRating.setRating(rating);
    		
    		getUserService().updateRating(userRating);
    	}
    	return Response.ok().build();
    }
    
    @GET
    @Path("/getActivityRating/{userId}/{activityId}")
    public Response getActivityRating(@PathParam("userId") Long userId, @PathParam("activityId") Long activityId) {

    	Rating userRating = getUserService().retrieveRating(userId, activityId);
    	
    	int score = 1;
    	if (userRating != null) {
    		score = userRating.getRating();
    	}
    	
    	return Response.ok(score).build();
    }
    
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}