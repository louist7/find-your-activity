package fya.rest;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;

import com.fasterxml.jackson.core.JsonProcessingException;

import fya.hibernate.User;
import fya.service.ActivityService;
import fya.service.UserService;

@Path(ActivityApplication.SEARCH_RESOURCE_ROOT)
@RequestScoped
public class SearchResource {

	@Inject
	private ActivityService activityService;
	@Inject
	private UserService userService;
	
	@GET
    @Path("/getActivityByName/{name}")
    public Response getActivityByName(@PathParam("name") String name) throws JsonProcessingException {	
        
    	return Response.ok(getActivityService().retrieveByName(name)).build();
    }
    
    @GET
    @Path("/getActivityByID/{id}")
    public Response getActivityByID(@PathParam("id") Long id) {
    	return Response.ok(getActivityService().getById(id)).build();
    }
    
    @GET
    @Path("/getHighestRatedActivities")
    public Response getHighestRatedActivities() {
        
    	return Response.ok(getActivityService().calculateAllBestRatedActivities().toString()).build();
    }
    
    @POST
    @Path("/getUserHighestRatedActivities")
    public Response getUserHighestRatedActivities(@RequestBody User user) {
        
    	if (user.getPostcode() == null || user.getPostcode().isEmpty()) {
    		return Response.noContent().build();
    	}
    	
    	List<JSONObject> userList = getUserService().calculateSimilarUsers(user, user.getRangeInMiles(), true);

    	if (userList == null) {
    		return Response.noContent().build();
    	}
    	
    	Object[] usersArray = userList.toArray();
    	
    	for (int a = 0; a < usersArray.length; a++) {
    		for (int b = 1; b < usersArray.length - a; b++) {
    			
        		JSONObject currentObj = (JSONObject) usersArray[b-1];
    			JSONObject obj = (JSONObject) usersArray[b];

				if (currentObj.getDouble("coefficient") < obj.getDouble("coefficient")) {
					usersArray[b-1] = obj;
					usersArray[b] = currentObj;
				}
    		}
    	}
    	
    	List<Long> ids = new ArrayList<Long>();
    	for (int a = 0; a < usersArray.length; a++) {
    		if (a > 9) {
    			break;
    		}
    		JSONObject obj = (JSONObject) usersArray[a];
    		Long id = (Long) obj.get("id");
    		ids.add(id);
    	}
    	
    	if (ids.isEmpty()) {
    		return Response.noContent().build();
    	}
    	
    	return Response.ok(getActivityService().calculateBestRatedActivitiesForUsers(ids).toString()).build();
    }
    
    @POST
    @Path("/getActivitiesNear")
    public Response getActivitiesNear(@RequestBody User user) {
    	
    	if (user.getPostcode() == null || user.getPostcode().isEmpty()) {
    		return Response.noContent().build();
    	}
        
    	return Response.ok(getActivityService().calculateNearestActivities(user.getPostcode()).toString()).build();
    }

	public ActivityService getActivityService() {
		return activityService;
	}

	public void setActivityService(ActivityService activityService) {
		this.activityService = activityService;
	}
	
    public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}