package fya.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;

import fya.hibernate.User;
import fya.service.ActivityService;
import fya.service.UserService;

@Path(ActivityApplication.HOME_RESOURCE_ROOT)
@RequestScoped
public class HomeResource {

	@Inject
	private ActivityService activityService;
	@Inject
	private UserService userService;
	
	@GET
	@Path("/retrieveUserData/{username}")
	public Response retrieveUserData(@PathParam("username") String username) {
		
		User user = getUserService().retrieveByUsername(username);
		return Response.ok(user).build();
	}
	
	@POST
	@Path("/guestSearch")
    public Response guestSearch(@RequestBody User user) {
		
		user.setPostcode(user.getPostcode().replaceAll("\\s", ""));
		
		return calculateResults(user);
	}
	
	@POST
	@Path("/userSearch/{username}")
    public Response userSearch(@PathParam("username") String username, @RequestBody User user) {

		user.setPostcode(user.getPostcode().replaceAll("\\s", ""));
		
		getUserService().updateUserDetails(user);
		
		return calculateResults(user);
	}
	
    private Response calculateResults(User user) {
    	
    	List<JSONObject> results = getActivityService().calculateActivityResults(user); 
    	
    	if (results == null) {
    		return Response.noContent().build();
    	}
    	
        return Response.ok(results.toString()).build();
    }
    
    public void setActivityService(ActivityService activityService) {
    	this.activityService = activityService;
    }
    
    public ActivityService getActivityService() {
		return this.activityService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
