package fya.rest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.springframework.web.bind.annotation.RequestBody;

import fya.hibernate.Activity;
import fya.service.ActivityService;

@Path(ActivityApplication.ADMIN_RESOURCE_ROOT)
@RequestScoped
public class AdminResource {

	@Inject
	private ActivityService activityService;
 
    @POST
    @Path("/addActivity")
    public Response addActivity(@RequestBody Activity activity) {

    	activity.setPostcode(activity.getPostcode().replaceAll("\\s", ""));
    	
    	getActivityService().save(activity);
    	return Response.ok().build();
    }
    
    @POST
    @Path("/updateActivity")
    public Response updateActivity(@RequestBody Activity activity) {

    	activity.setPostcode(activity.getPostcode().replaceAll("\\s", ""));
    	
    	getActivityService().update(activity);
    	return Response.ok().build();
    }
    
    @POST
    @Path("/deleteActivity")
    public Response deleteActivity(@RequestBody Activity activity) {

    	getActivityService().delete(activity);
    	return Response.ok().build();
    }

	public ActivityService getActivityService() {
		return activityService;
	}

	public void setActivityService(ActivityService activityService) {
		this.activityService = activityService;
	}
}