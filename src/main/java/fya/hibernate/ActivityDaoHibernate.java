package fya.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

public class ActivityDaoHibernate extends BaseDao implements ActivityDao<Activity> {

	public ActivityDaoHibernate() {

	}

	@Override
	public void store(Activity activity) {
		openCurrentSessionwithTransaction();
		
		getCurrentSession().save(activity);
		getCurrentSession().flush();
		
		closeCurrentSessionwithTransaction();
	}
	
	@Override
	public void merge(Activity activity) {
		openCurrentSessionwithTransaction();
		
		getCurrentSession().merge(activity);
		getCurrentSession().flush();
		
		closeCurrentSessionwithTransaction();
	}

	@Override
	public void delete(Activity activity) {
		openCurrentSessionwithTransaction();
		
		getCurrentSession().delete(activity);;
		getCurrentSession().flush();
		
		closeCurrentSessionwithTransaction();
	}

	@Override
	public List<Activity> getAllActivities() {
		openCurrentSession();
		Criteria criteria = getCurrentSession().createCriteria(Activity.class);
		List<Activity> activities = (List<Activity>) criteria.list();
		closeCurrentSession();
		return activities;
	}

	@Override
	public List<Activity> retrieveByName(String name) {
		openCurrentSession();
		Criteria criteria = getCurrentSession().createCriteria(Activity.class);
		criteria.add(Restrictions.like("name", name, MatchMode.ANYWHERE));
		List<Activity> activities = (List<Activity>) criteria.list();
		closeCurrentSession();
		return activities;
	}

	@Override
	public Activity getById(Long id) {
		openCurrentSession();
		Criteria criteria = getCurrentSession().createCriteria(Activity.class);
		criteria.add(Restrictions.eq("id", id));
		Activity activity = (Activity) criteria.uniqueResult();
		closeCurrentSession();
		return activity;
	}
}
