package fya.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

public class RatingDaoHibernate extends BaseDao implements RatingDao<Rating> {

	public RatingDaoHibernate() {

	}

	@Override
	public Rating getById(Long id) {
		openCurrentSessionwithTransaction();
		
		Criteria criteria = getCurrentSession().createCriteria(Rating.class);
		criteria.add(Restrictions.eq("id", id));
		Rating rating = (Rating) criteria.list().get(0);
		closeCurrentSessionwithTransaction();
		return rating;
	}
	
	@Override
	public void store(Rating rating) {
		openCurrentSessionwithTransaction();
		
		getCurrentSession().save(rating);
		getCurrentSession().flush();
		
		closeCurrentSessionwithTransaction();
	}
	
	@Override
	public void merge(Rating rating) {
		openCurrentSessionwithTransaction();
		
		getCurrentSession().merge(rating);
		getCurrentSession().flush();
		
		closeCurrentSessionwithTransaction();
	}

	@Override
	public Rating retrieve(Long userId, Long activityId) {
		
		openCurrentSessionwithTransaction();
		
		Criteria criteria = getCurrentSession().createCriteria(Rating.class);
		criteria.add(Restrictions.eq("userId", userId));
		criteria.add(Restrictions.eq("activityId", activityId));
		if (criteria.list().isEmpty()) {
			closeCurrentSessionwithTransaction();
			return null;
		} else {
			Rating rating = (Rating) criteria.list().get(0);
			closeCurrentSessionwithTransaction();
			return rating;
		}
	}

	@Override
	public List<Rating> retrieveRatingsForActivity(Long activityId) {
		openCurrentSession();
		
		Criteria criteria = getCurrentSession().createCriteria(Rating.class);
		criteria.add(Restrictions.eq("activityId", activityId));
		if (criteria.list().isEmpty()) {
			closeCurrentSession();
			return null;
		} else {
			List<Rating> ratings = (List<Rating>) criteria.list();
			closeCurrentSession();
			return ratings;
		}
	}

	@Override
	public List<Rating> retrieveRatingsForActivityAndUsers(Long activityId, List<Long> userIds) {
		openCurrentSession();
		
		Criteria criteria = getCurrentSession().createCriteria(Rating.class);
		criteria.add(Restrictions.eq("activityId", activityId));
		criteria.add(Restrictions.in("userId", userIds));
		
		if (criteria.list().isEmpty()) {
			closeCurrentSession();
			return null;
		} else {
			List<Rating> ratings = (List<Rating>) criteria.list();
			closeCurrentSession();
			return ratings;
		}
	}
}
