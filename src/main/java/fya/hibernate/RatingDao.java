package fya.hibernate;

import java.util.List;

public interface RatingDao<T> {

	public Rating getById(Long id);
	
	public void store(Rating rating);
	
	public void merge(Rating rating);
	
	public Rating retrieve(Long userId, Long activityId);
	
	public List<Rating> retrieveRatingsForActivity(Long activityId);
	
	public List<Rating> retrieveRatingsForActivityAndUsers(Long activityId, List<Long> userIds);
}
