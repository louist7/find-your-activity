package fya.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

public class UserDaoHibernate extends BaseDao implements UserDao<User> {

	public UserDaoHibernate() {

	}

	@Override
	public void store(User user) {
		openCurrentSessionwithTransaction();

		getCurrentSession().save(user);
		getCurrentSession().flush();
		
		closeCurrentSessionwithTransaction();
	}

	@Override
	public User checkLogin(String username, String password) {
		
		openCurrentSession();
		
		Criteria criteria = getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("username", username));
		criteria.add(Restrictions.eq("password", password));
		if (criteria.list().isEmpty()) {
			closeCurrentSession();
			return null;
		} else {
			User user = (User) criteria.list().get(0);
			closeCurrentSession();
			return user;
		}
	}
	
	public Boolean checkUsernameExists(String username) {
		
		openCurrentSession();
		
		Criteria criteria = getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("username", username));
		if (criteria.list().isEmpty()) {
			closeCurrentSession();
			return false;
		} else {
			closeCurrentSession();
			return true;
		}
	}

	@Override
	public User retrieveByUserName(String username) {
		openCurrentSession();
		Criteria criteria = getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("username", username));
		User user = (User) criteria.list().get(0);
		closeCurrentSession();
		return user;
	}

	@Override
	public void update(User user) {
		openCurrentSessionwithTransaction();

		getCurrentSession().merge(user);
		getCurrentSession().flush();
		
		closeCurrentSessionwithTransaction();
	}
	
	@Override
	public List<User> getAllUsers() {
		openCurrentSession();
		Criteria criteria = getCurrentSession().createCriteria(User.class);
		List<User> users = (List<User>) criteria.list();
		closeCurrentSession();
		return users;
	}

	@Override
	public User retrieve(Long id) {
		openCurrentSession();
		Criteria criteria = getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("id", id));
		User user = (User) criteria.list().get(0);
		closeCurrentSession();
		return user;
		
	}
}
