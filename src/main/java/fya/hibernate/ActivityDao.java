package fya.hibernate;

import java.util.List;

public interface ActivityDao<T> {

	public void store(Activity activity);
	
	public List<Activity> getAllActivities();
	
	public List<Activity> retrieveByName(String name);
	
	public Activity getById(Long id);
	
	public void merge(Activity activity);
	
	public void delete(Activity activity);
}
