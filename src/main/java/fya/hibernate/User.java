package fya.hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
	
	@Column(name = "username")
	private String username;
	@Column(name = "password")
	private String password;
	@Column(name = "interests")
	private String interests;
	@Column(name = "fitness_level")
	private String fitnessLevel;
	@Column(name = "age_range")
	private String ageRange;
	@Column(name = "no_of_participants")
	private Integer noOfParticipants;
	@Column(name = "postcode")
	private String postcode;
	@Column(name = "range_in_miles")
	private Integer rangeInMiles;
	@Column(name = "budget_range")
	private String budgetRange;
	@Column(name = "admin")
	private boolean admin;
	@Column(name = "email")
	private String email;
	@Column(name = "visible_profile")
	private boolean visibleProfile;
	@Column(name = "phone")
	private String phone;
	
	public User() {
	}
	
	public Long getId() {
		return id;
	}

	private void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getInterests() {
		return interests;
	}

	public void setInterests(String interests) {
		this.interests = interests;
	}

	public String getFitnessLevel() {
		return fitnessLevel;
	}

	public void setFitnessLevel(String fitnessLevel) {
		this.fitnessLevel = fitnessLevel;
	}

	public String getAgeRange() {
		return ageRange;
	}

	public void setAgeRange(String ageRange) {
		this.ageRange = ageRange;
	}

	public Integer getNoOfParticipants() {
		return noOfParticipants;
	}

	public void setNoOfParticipants(Integer noOfParticipants) {
		this.noOfParticipants = noOfParticipants;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public Integer getRangeInMiles() {
		return rangeInMiles;
	}

	public void setRangeInMiles(Integer rangeInMiles) {
		this.rangeInMiles = rangeInMiles;
	}

	public String getBudgetRange() {
		return budgetRange;
	}

	public void setBudgetRange(String budgetRange) {
		this.budgetRange = budgetRange;
	}
	
	public boolean isAdmin() {
		return admin;
	}
	
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isVisibleProfile() {
		return visibleProfile;
	}

	public void setVisibleProfile(boolean visibleProfile) {
		this.visibleProfile = visibleProfile;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
}