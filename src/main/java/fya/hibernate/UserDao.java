package fya.hibernate;

import java.util.List;

public interface UserDao<T> {

	public void store(User user);
	
	public User checkLogin(String username, String password);
	
	public Boolean checkUsernameExists(String username);
	
	public User retrieveByUserName(String username);
	
	public void update(User user);
	
	public User retrieve(Long id);
	
	public List<User> getAllUsers();
}
