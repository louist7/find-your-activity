package fya.service;

import java.util.List;

import org.json.JSONObject;

import fya.hibernate.Rating;
import fya.hibernate.User;

public interface UserService {
	
	public User login(String username, String password);
	
	public User createUser(String username, String password);
	
	public List<JSONObject> calculateSimilarUsers(User user, int distance, boolean includeDistance);
	
	public User retrieveByUsername(String username);
	
	public void updateUserDetails(User user);
	
	public void storeRating(Rating rating);
	
	public void updateRating(Rating rating);
	
	public Rating retrieveRating(Long userId, Long activityId);
}
