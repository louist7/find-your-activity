package fya.service;

import fya.hibernate.Activity;
import fya.hibernate.User;

public class SimilarityServiceImpl implements SimilarityService {

	public SimilarityServiceImpl() {
		
	}
	
	@Override
	public Double calculatePearsonCorrelation(int[] array1, int[] array2) {
		// returns the Pearson's correlation coefficient for array1 and array2
		
		int numOfValues = array1.length;
		
		int sumArray1 = 0;
		for (int i : array1) {
			sumArray1 = sumArray1 + i;
		}
		
		int sumArray2 = 0;
		for (int i : array2) {
			sumArray2 = sumArray2 + i;
		}
		
		int sumArray1Sq = 0;
		for (int i : array1) {
			sumArray1Sq = sumArray1Sq + (i * i);
		}
		
		int sumArray2Sq = 0;
		for (int i : array2) {
			sumArray2Sq = sumArray2Sq + (i * i);
		}
		
		int productsSum = 0;		
		for (int i = 0; i < array1.length; i++) {
			int product = array1[i] * array2[i];
			productsSum = productsSum + product;
		}
		
		double num = productsSum - (sumArray1 * sumArray2 / numOfValues);
		double denArray1 = sumArray1Sq - (sumArray1 *  sumArray1) / numOfValues;
		double denArray2 = sumArray2Sq - (sumArray2 *  sumArray2) / numOfValues;
		double den = Math.sqrt(denArray1 * denArray2);
		
		if (den == 0) {
			return null;
		}
		
		double coefficient = num / den;
		return coefficient;
	}
	
	@Override
	public int[] userToIntArray(User user) {
		return convertToIntArray(user.getInterests(),user.getFitnessLevel(),user.getAgeRange(),
				user.getNoOfParticipants(),user.getBudgetRange());
	}
	
	@Override
	public int[] activityToIntArray(Activity activity) {
		return convertToIntArray(activity.getActivityType(),activity.getFitnessRequired(),activity.getAgeRange(),
				activity.getNoOfParticipants(),activity.getPriceRange());
	}
	
	private int[] convertToIntArray(String interests, String fitnessLevels, String ageRange, int numOfParticipants, String budget) {
		String[] interestOptions = {"Relaxing","Indoor Sports","Team Sports","Outdoor Sports","Adventure/Interesting"};
		String[] fitnessLevelsOptions = {"Not Much Fitness","Some Fitness","Reasonable Fitness","Good Fitness","Very Good Fitness"};
		String[] ageRangeOptions = {"Under 18","18-24","25-44","45-64","65+"};
		int[] numOfParticipantsOptions = {1,2,3,4,5};
		String[] budgetOptions = {"Under £20","£21-£40","£41-£60","£61-£80","£81+"};
		
		int interestsPos = 0;
		for (int i = 0; i < interestOptions.length; i++) {
			if (interests.equals(interestOptions[i])) {
				interestsPos = i+1;
			}
		}
		
		int fitnessLevelsPos = 0;
		for (int i = 0; i < fitnessLevelsOptions.length; i++) {
			if (fitnessLevels.equals(fitnessLevelsOptions[i])) {
				fitnessLevelsPos = i+1;
			}
		}
		
		int ageRangePos = 0;
		for (int i = 0; i < ageRangeOptions.length; i++) {
			if (ageRange.equals(ageRangeOptions[i])) {
				ageRangePos = i+1;
			}
		}
		
		int numOfParticipantsPos = 0;
		for (int i = 0; i < numOfParticipantsOptions.length; i++) {
			if (numOfParticipants == numOfParticipantsOptions[i]) {
				numOfParticipantsPos = i+1;
			}
		}
		
		int budgetPos = 0;
		for (int i = 0; i < budgetOptions.length; i++) {
			if (budget.equals(budgetOptions[i])) {
				budgetPos = i+1;
			}
		}
		
		int[] array = {interestsPos,fitnessLevelsPos,ageRangePos,numOfParticipantsPos,budgetPos};
		return array;
	}
}
