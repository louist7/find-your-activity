package fya.service;

import fya.hibernate.Activity;
import fya.hibernate.User;

public interface SimilarityService {

	public Double calculatePearsonCorrelation(int[] array1, int[] array2);
	
	public int[] userToIntArray(User user);
	
	public int[] activityToIntArray(Activity activity);
}
