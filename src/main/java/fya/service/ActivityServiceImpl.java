package fya.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.inject.Model;

import org.json.JSONArray;
import org.json.JSONObject;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import fya.hibernate.Activity;
import fya.hibernate.ActivityDaoHibernate;
import fya.hibernate.Rating;
import fya.hibernate.RatingDaoHibernate;
import fya.hibernate.User;

@Model
public class ActivityServiceImpl implements ActivityService {
	
	private static ActivityDaoHibernate activityDao = new ActivityDaoHibernate();
	private static RatingDaoHibernate ratingDao = new RatingDaoHibernate();
	
	private SimilarityService similarityService = new SimilarityServiceImpl();

	private static String KEY = "AIzaSyDFOXwZsxodD3b7I_SPZ7BqSdFvp1SnVTs";
	private static String BASE_URL = "https://maps.googleapis.com/maps/api/distancematrix/json?key="+KEY;
	
	@Override
	public List<JSONObject> calculateActivityResults(User user) {
		//will return the activities with the 10 best Pearson correlation coefficients
		
		//initial filter of the activities by location
		List<Activity> activities = filterByLocationPreferences(user.getPostcode(), user.getRangeInMiles());

		List<JSONObject> activitiesJSON = new ArrayList<JSONObject>(); 
		
		for (Activity activity : activities) {
			Double coefficient = getSimilarityService().calculatePearsonCorrelation(getSimilarityService().userToIntArray(user), getSimilarityService().activityToIntArray(activity));
			
			if (coefficient == null) {
				return null;
			}
			
			JSONObject obj = new JSONObject(activity);
			obj.put("coefficient", coefficient);
			
			activitiesJSON.add(obj);
		}
		
		return activitiesJSON;
	}
	
	private List<Activity> filterByLocationPreferences(String postcode, int rangeInMiles) {
		/* This uses the external Google Maps Distance Matrix API.
		*  This API has been enabled for this project in the Google Developers Console under my account.
		*  This requires the authentication key: AIzaSyDFOXwZsxodD3b7I_SPZ7BqSdFvp1SnVTs 
		*  The API manager can be viewed at: https://console.developers.google.com/apis/library?project=fypfya
		*/
		HttpClient httpClient = new DefaultHttpClient();
		
		List<Activity> filteredActivities = new ArrayList<Activity>();
		
	 	for (Activity activity : getActivityDao().getAllActivities()) {
			boolean beingAdded = true;
	 		try {
				HttpGet getRequest = new HttpGet(BASE_URL + "&origins=" + postcode + "&destinations=" + activity.getPostcode());
				HttpResponse response = httpClient.execute(getRequest);
				
				InputStream is = response.getEntity().getContent();
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				StringBuilder stringBuilder = new StringBuilder();
				String inputStr;
				while ((inputStr = bufferedReader.readLine()) != null) {
					stringBuilder.append(inputStr);
				}
				
				JSONObject jsonResponse = new JSONObject(stringBuilder.toString());
				JSONArray rowsArray = jsonResponse.getJSONArray("rows");
				JSONArray elementsArray = rowsArray.getJSONObject(0).getJSONArray("elements");
				JSONObject distanceObject = elementsArray.getJSONObject(0).getJSONObject("distance");
				
				String kmString = distanceObject.getString("text");
				String km = (kmString.substring(0, kmString.length() - 3)).replaceAll("\\s", "");
				
				double distanceKM = Double.valueOf(km);

				double distanceMiles = distanceKM * 0.621371;
				if (distanceMiles > rangeInMiles) {
					beingAdded = false;
				}
				
			} catch(Exception e) {
				e.printStackTrace();
			}
	 		
	 		if (beingAdded) {
	 			filteredActivities.add(activity);
	 		}
		}
	 	httpClient.getConnectionManager().shutdown();
		return filteredActivities;
	}
	
	
	@Override
	public void save(Activity activity) {
		getActivityDao().store(activity);
	}
	
	@Override
	public void update(Activity activity) {
		getActivityDao().merge(activity);
	}

	@Override
	public void delete(Activity activity) {
		getActivityDao().delete(activity);
	}
	
	@Override
	public List<Activity> retrieveByName(String name) {
		return getActivityDao().retrieveByName(name);
	}

	@Override
	public Activity getById(Long id) {
		return getActivityDao().getById(id);
	}
	
	@Override
	public List<JSONObject> calculateBestRatedActivitiesForUsers(List<Long> userIds) {
		return calculateBestRatedActivities(userIds);
	}
	
	@Override
	public List<JSONObject> calculateAllBestRatedActivities() {
		return calculateBestRatedActivities(null);
	}
	
	private List<JSONObject> calculateBestRatedActivities(List<Long> userIds) {
		
		List<JSONObject> activitiesJSON = new ArrayList<JSONObject>(); 
		for (Activity activity : getActivityDao().getAllActivities()) {
			JSONObject obj = new JSONObject(activity);
			
			List<Rating> ratings;
			if (userIds == null) {
				ratings = getRatingDao().retrieveRatingsForActivity(activity.getId());
			} else {
				ratings = getRatingDao().retrieveRatingsForActivityAndUsers(activity.getId(), userIds);
			}
			
			if (ratings != null) {
				Double rating = calclulateAverageRating(ratings);
				obj.put("averageRating", rating);
				obj.put("numOfRatings", ratings.size());
				
			} else {
				obj.put("averageRating", 0);
				obj.put("numOfRatings", 0);
			}
			activitiesJSON.add(obj);
		}
		
		return activitiesJSON;
	}
	
	private Double calclulateAverageRating(List<Rating> ratings) {

		int num = ratings.size();
		int total = 0;
		for (Rating rating: ratings) {
			total = total + rating.getRating();
		}
		
		Double average = Double.valueOf(total) / Double.valueOf(num);
		
		return average;
	}
	
	@Override
	public List<JSONObject> calculateNearestActivities(String postcode) {

		HttpClient httpClient = new DefaultHttpClient();
		
		List<JSONObject> filteredActivities = new ArrayList<JSONObject>();
		
	 	for (Activity activity : getActivityDao().getAllActivities()) {
	 		try {
				HttpGet getRequest = new HttpGet(BASE_URL + "&origins=" + postcode + "&destinations=" + activity.getPostcode());
				HttpResponse response = httpClient.execute(getRequest);
				
				InputStream is = response.getEntity().getContent();
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				StringBuilder stringBuilder = new StringBuilder();
				String inputStr;
				while ((inputStr = bufferedReader.readLine()) != null) {
					stringBuilder.append(inputStr);
				}
				
				JSONObject jsonResponse = new JSONObject(stringBuilder.toString());
				JSONArray rowsArray = jsonResponse.getJSONArray("rows");
				JSONArray elementsArray = rowsArray.getJSONObject(0).getJSONArray("elements");
				JSONObject distanceObject = elementsArray.getJSONObject(0).getJSONObject("distance");
				
				String kmString = distanceObject.getString("text");
				String km = (kmString.substring(0, kmString.length() - 3)).replaceAll("\\s", "");
				
				double distanceKM = Double.valueOf(km);

				double distanceMiles = distanceKM * 0.621371;
				
				JSONObject obj = new JSONObject(activity);
				obj.put("distance", distanceMiles);
				filteredActivities.add(obj);
				
			} catch(Exception e) {
				e.printStackTrace();
			}
	 		
		}
	 	httpClient.getConnectionManager().shutdown();
		return filteredActivities;
	}
	
	public static ActivityDaoHibernate getActivityDao() {
		return activityDao;
	}

	public static void setActivityDao(ActivityDaoHibernate activityDao) {
		ActivityServiceImpl.activityDao = activityDao;
	}

	public SimilarityService getSimilarityService() {
		return similarityService;
	}

	public void setSimilarityService(SimilarityService similarityService) {
		this.similarityService = similarityService;
	}

	public static RatingDaoHibernate getRatingDao() {
		return ratingDao;
	}

	public static void setRatingDao(RatingDaoHibernate ratingDao) {
		ActivityServiceImpl.ratingDao = ratingDao;
	}
}
