package fya.service;

import java.util.List;

import org.json.JSONObject;

import fya.hibernate.Activity;
import fya.hibernate.User;

public interface ActivityService {    

    public List<JSONObject> calculateActivityResults(User user);
    
    public List<Activity> retrieveByName(String name);
    
    public Activity getById(Long id);
    
    public void save(Activity activity);
    
    public void update(Activity activity);
    
    public void delete(Activity activity);
    
    public List<JSONObject> calculateBestRatedActivitiesForUsers(List<Long> userIds);
    
    public List<JSONObject> calculateAllBestRatedActivities();
    
    public List<JSONObject> calculateNearestActivities(String postcode);
}
