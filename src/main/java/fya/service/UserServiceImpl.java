package fya.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.inject.Model;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import fya.hibernate.Rating;
import fya.hibernate.RatingDaoHibernate;
import fya.hibernate.User;
import fya.hibernate.UserDaoHibernate;

@Model
public class UserServiceImpl implements UserService {

	private static UserDaoHibernate userDao = new UserDaoHibernate();
	private static RatingDaoHibernate ratingDao = new RatingDaoHibernate();
	
	private SimilarityService similarityService = new SimilarityServiceImpl();

	@Override
	public List<JSONObject> calculateSimilarUsers(User user, int distance, boolean includeDistance) {
		//will return the activities with the 10 best Pearson correlation coefficients
		
		List<User> users;
		if (includeDistance) {
			users = filterUsersByLocationInMiles(user.getPostcode(), distance);
		} else {
			users = getUserDao().getAllUsers();
		}
		
		List<JSONObject> usersJSON = new ArrayList<JSONObject>();
		for (User currentUser: users) {
			if (!currentUser.getId().equals(user.getId()) && currentUser.isVisibleProfile()) {
				Double coefficient = getSimilarityService().calculatePearsonCorrelation(getSimilarityService().userToIntArray(user), getSimilarityService().userToIntArray(currentUser));

				if (coefficient == null) {
					return null;
				}
				
				JSONObject obj = new JSONObject(currentUser);
				obj.put("coefficient", coefficient);
				
				usersJSON.add(obj);
			}
		}
		return usersJSON;
	}
	
	private List<User> filterUsersByLocationInMiles(String postcode, int rangeInMiles) {
		/* This uses the external Google Maps Distance Matrix API.
		*  This API has been enabled for this project in the Google Developers Console under my account.
		*  This requires the authentication key: AIzaSyDFOXwZsxodD3b7I_SPZ7BqSdFvp1SnVTs 
		*  The API manager can be viewed at: https://console.developers.google.com/apis/library?project=fypfya
		*/
		String key = "AIzaSyDFOXwZsxodD3b7I_SPZ7BqSdFvp1SnVTs";
		String baseUrl = "https://maps.googleapis.com/maps/api/distancematrix/json?key="+key;
		HttpClient httpClient = new DefaultHttpClient();
		
		List<User> filteredUsers = new ArrayList<User>();
		
	 	for (User user : getUserDao().getAllUsers()) {
			boolean beingAdded = true;
			
			if (user.getPostcode() == null || user.getPostcode().isEmpty()) {
				beingAdded = false;
			} else {
				try {
					HttpGet getRequest = new HttpGet(baseUrl + "&origins=" + postcode + "&destinations=" + user.getPostcode());
					HttpResponse response = httpClient.execute(getRequest);
					
					InputStream is = response.getEntity().getContent();
					BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
					StringBuilder stringBuilder = new StringBuilder();
					String inputStr;
					while ((inputStr = bufferedReader.readLine()) != null) {
						stringBuilder.append(inputStr);
					}
					
					JSONObject jsonResponse = new JSONObject(stringBuilder.toString());
					JSONArray rowsArray = jsonResponse.getJSONArray("rows");
					JSONArray elementsArray = rowsArray.getJSONObject(0).getJSONArray("elements");
					JSONObject distanceObject = elementsArray.getJSONObject(0).getJSONObject("distance");
					
					String kmString = distanceObject.getString("text");
					String km = (kmString.substring(0, kmString.length() - 3)).replaceAll("\\s", "");
					
					double distanceKM = Double.valueOf(km);
					
					double distanceMiles = distanceKM * 0.621371;
					if (distanceMiles > rangeInMiles) {
						beingAdded = false;
					}
					
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
	 		
	 		if (beingAdded) {
	 			filteredUsers.add(user);
	 		}
		}
	 	httpClient.getConnectionManager().shutdown();
		return filteredUsers;
	}
	
	@Override
	public User login(String username, String password) {
		return getUserDao().checkLogin(username, password);
	}

	@Override
	public User createUser(String username, String password) {
		
		if (getUserDao().checkUsernameExists(username)) {
			//username exists
			return null;
		}
		
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setAdmin(false);
		user.setEmail("");
		user.setPhone("");
		user.setVisibleProfile(true);
		getUserDao().store(user);
		return user;
	}
	
	@Override
	public User retrieveByUsername(String username) {
		return getUserDao().retrieveByUserName(username);
	}
	
	@Override
	public void updateUserDetails(User user) {
		getUserDao().update(user);
	}
	
	@Override
	public void storeRating(Rating rating) {
		getRatingDao().store(rating);
	}
	
	@Override
	public void updateRating(Rating rating) {
		getRatingDao().merge(rating);
	}
	
	@Override
	public Rating retrieveRating(Long userId, Long activityId) {
		return getRatingDao().retrieve(userId, activityId);
	}
	
	public static UserDaoHibernate getUserDao() {
		return userDao;
	}

	public static void setUserDao(UserDaoHibernate userDao) {
		UserServiceImpl.userDao = userDao;
	}
	
	public static RatingDaoHibernate getRatingDao() {
		return ratingDao;
	}

	public static void setRatingDao(RatingDaoHibernate ratingDao) {
		UserServiceImpl.ratingDao = ratingDao;
	}

	public SimilarityService getSimilarityService() {
		return similarityService;
	}

	public void setSimilarityService(SimilarityService similarityService) {
		this.similarityService = similarityService;
	}
}
