var ngApp = angular.module('ngApp', [
   'ngRoute',
   'ngStorage',
   'controllers',
   'schemaForm',
   'feature-flags',
   'ui.bootstrap'
]);

ngApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/home', {
		templateUrl : 'views/home.html',
		controller : 'HomeController'
	}).when('/about', {
		templateUrl : 'views/about.html',
		controller : 'AboutController'
	}).when('/activitysearch', {
		templateUrl : 'views/search.html',
		controller : 'SearchController'
	}).when('/user', {
		templateUrl : 'views/login.html',
		controller : 'LoginController'
	}).when('/details/:id', {
		templateUrl : 'views/details.html',
		controller : 'DetailsController'
	}).when('/admin', {
		templateUrl : 'views/admin.html',
		controller : 'AdminController'
	}).otherwise({
		redirectTo : '/home'
	});
}])

.run(['featureFlags', 'loginService',
    function(featureFlags, loginService) {
	
	if (loginService.localStorage.isLoggedIn) {
		if (loginService.localStorage.user.admin) {
			var flagData = [{"key": "admin", "active": true}];
	        featureFlags.set(flagData);
		} else {
			var flagData = [{"key": "admin", "active": false}];
	        featureFlags.set(flagData);
		}
	} else {
		var flagData = [{"key": "admin", "active": false}];
        featureFlags.set(flagData);
	}
}]);
