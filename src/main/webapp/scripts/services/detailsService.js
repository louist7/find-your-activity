services.service('detailsService', ['$http', function($http) {
	
	var resourceBaseURL = 'resource/search';
	
	var getActivityDetails = function(id) {
		return $http.get(resourceBaseURL + '/getActivityByID/' + id);
	};
	
	var saveActivityRating = function(userId, activityId, rating) {
		return $http.post('resource/user' + '/saveActivityRating/' + userId +'/'+ activityId +'/'+ rating);
	};
	
	var getRating = function(userId, activityId) {
		return $http.get('resource/user' + '/getActivityRating/' + userId +'/'+ activityId);
	};
	
	return {
		getActivityDetails: getActivityDetails,
		saveActivityRating: saveActivityRating,
		getRating: getRating
	};
}]);