services.service('loginService', ['$http', '$localStorage', '$sessionStorage', function($http, $localStorage, $sessionStorage) {
	
	var resourceBaseURL = 'resource/user';
	var localStorage = $localStorage;
	
	var login = function(loginForm) {
		return $http.post(resourceBaseURL + '/login', loginForm);
	};
	
	var signup = function(signupForm) {
		return $http.post(resourceBaseURL + '/signup', signupForm);
	};
	
	var findSimilarUsers = function(user, userDistance) {
		return $http.post(resourceBaseURL + '/findSimilarUsers/' + userDistance, user);
	};
	
	var updateProfile = function(username, details) {
		return $http.post(resourceBaseURL + '/updateUserProfile/' + username, details);
	};
	
	return {
		localStorage: localStorage,
		login: login,
		signup: signup,
		findSimilarUsers: findSimilarUsers,
		updateProfile: updateProfile
	};
}]);