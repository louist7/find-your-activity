services.service('adminService', ['$http', function($http) {
	
	var resourceBaseURL = 'resource/admin';
	
	var logNew = function(activityForm) {
		return $http.post(resourceBaseURL + '/addActivity', activityForm);
	};
	
	var updateExisting = function(activityForm) {
		return $http.post(resourceBaseURL + '/updateActivity', activityForm);
	};
	
	var deleteActivity = function(activity) {
		return $http.post(resourceBaseURL + '/deleteActivity', activity);
	}
	
	return {
		logNew: logNew,
		updateExisting: updateExisting,
		deleteActivity: deleteActivity
	};
}]);