services.service('homeService', ['$http', function($http) {
	
	var resourceBaseURL = 'resource/home';
	
	var calculateResultsForGuest = function(activityForm) {
		return $http.post(resourceBaseURL + '/guestSearch', activityForm);
	};
	
	var calculateResultsForUser = function(activityForm, username) {
		return $http.post(resourceBaseURL + '/userSearch/' + username, activityForm);
	};
	
	var retrieveUserData = function(username) {
		return $http.get(resourceBaseURL + '/retrieveUserData/' + username);
	}
	
	return {
		calculateResultsForGuest: calculateResultsForGuest,
		calculateResultsForUser: calculateResultsForUser,
		retrieveUserData: retrieveUserData
	};
}]);