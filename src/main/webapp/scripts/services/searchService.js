services.service('searchService', ['$http', function($http) {
	
	var resourceBaseURL = 'resource/search';
	
	var search = function(name) {
		return $http.get(resourceBaseURL + '/getActivityByName/' + name);
	};
	
	var highestRated = function() {
		return $http.get(resourceBaseURL + '/getHighestRatedActivities');
	};
	
	var usersHighestRated = function(user) {
		return $http.post(resourceBaseURL + '/getUserHighestRatedActivities', user);
	};
	
	var nearActivities = function(user) {
		return $http.post(resourceBaseURL + '/getActivitiesNear', user);
	};
	
	return {
		search: search,
		highestRated: highestRated,
		usersHighestRated: usersHighestRated,
		nearActivities: nearActivities
	};
}]);