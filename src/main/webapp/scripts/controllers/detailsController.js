controllers.controller('DetailsController', ['$scope', 'detailsService', 'loginService', function ($scope, detailsService, loginService) {
	 
	
	$scope.ratingSaved = false;
	$scope.isLoggedIn = loginService.localStorage.isLoggedIn;
	
	$scope.loadDetails = function() {
		 $scope.activityID = location.hash.substr(10);
		 
		 detailsService.getActivityDetails($scope.activityID).success(function(data) {
			$scope.activityDetails = data;
			renderMap();
		 });
		
		 if (loginService.localStorage.isLoggedIn) {
			 $scope.showRatingOption = true;
			 $scope.userID = loginService.localStorage.user.id;
			 
			 detailsService.getRating($scope.userID, $scope.activityID).success(function(data) {
				 
				 $scope.userRating = data.toString();
			 });
		 }
	 };
	 
	 $scope.saveRating = function() {
		 detailsService.saveActivityRating($scope.userID, $scope.activityID, $scope.userRating).success(function(data) {	
			 $scope.ratingSaved = true;
			 $scope.savedMessage = 'Activity Rating Saved';
		 });
	 };
	 
	 var renderMap = function() {
		 var geocoder = new google.maps.Geocoder();
		 geocoder.geocode({'address': $scope.activityDetails.postcode}, function(results, status) 
					{
					  if (status == google.maps.GeocoderStatus.OK) 
					  {				   
					      var latitude = results[0].geometry.location.lat();
					      var longitude = results[0].geometry.location.lng();
					      
					      var mapOptions = {
					       		  zoom: 17,
					       		  center: {lat: latitude, lng: longitude} 		
					      };
					        
					      var map = new google.maps.Map(document.getElementById('map'), mapOptions);
					      var marker = new google.maps.Marker({
					    	  position: mapOptions.center,
					       	  map: map 	
					      });
					  }
					});
     };
}]);