controllers.controller('NavbarController', ['$scope', function ($scope) {

	// function to toggle the menu on and off
	$scope.toggleMenu = function() {
		$("#wrapper").toggleClass("toggled");
	};
	
	// will add the correct padding to the body when the navbar resizes on smaller screens
	$(window).resize(function() {
	    $('body').css({"padding-top": $(".navbar").height() + "px"});
	});
}]);