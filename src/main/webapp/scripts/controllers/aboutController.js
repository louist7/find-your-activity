controllers.controller('AboutController', ['$scope', 'loginService', function ($scope, loginService) {

	$scope.userLoggedIn = loginService.localStorage.isLoggedIn;
	
	$scope.pageTitle = 'About Us';
	 
	$scope.mainContent = "Find Your Activity provides you with an opportunity to discover which " +
	 		"activities you are best suited to from a wide range of options. Use the activity calculator to " +
	 		"discover and rate your matches, enter your preferences and see what you get. " +
	 		"Try out the different search filters which provide a variety of ways " +
	 		"to find your best match, including showing the best rated activities by the users with similar " +
	 		"preferences to your own.";
	 
	$scope.makeTheMost = "Your user profile is key, it allows you to find like minded users " +
	 		"with whom collaborative suggestions for you next activity can be discovered. " +
	 		"In order to make the most of Find Your Activity create an account today.";
	 
	$scope.signUp = "Sign Up Here";
	 
}]);