controllers.controller('AdminController', ['$scope', 'adminService', 'searchService', function ($scope, adminService, searchService) {
	 
	 $scope.isLoggingNew = true;
	 $scope.activitySaved = false;
	 $scope.savedMessage = 'Activity Details Saved';
	 $scope.activityName = '';
	 $scope.resultsFound = false;
	 $scope.noResults = true;
	 $scope.noResultsMessage = 'No matches found';
	 
	 $scope.schema = {
			    type: "object",
			    properties: {
			      "name": {
			    	  title: "Name",
			    	  type: "string",
			    	  required: true,
			    	  validationMessage: "Field is required"
			      },"company": {
			    	  title: "Company",
			    	  type: "string",
			    	  required: true,
			    	  validationMessage: "Field is required"
			      },"website": {
			    	  title: "Website URL",
			    	  type: "string",
			    	  required: true,
			    	  validationMessage: "Field is required"
			      },"address": {
			    	  title: "Address",
			    	  type: "string",
			    	  required: true,
			    	  validationMessage: "Field is required"
			      },"postcode": {
			    	  title: "Postcode",
			    	  type: "string",
			    	  required: true,
			    	  validationMessage: "Not a Valid UK Postcode"
			      },"price": {
			    	  title: "Price",
			    	  type: "string",
			    	  required: true,
			    	  validationMessage: "Field is required"
			      },"activityType": {
			    	  title: "Activity Type",
			    	  type: "string",
			    	  enum: ['Relaxing', 'Indoor Sports', 'Team Sports', 'Outdoor Sports', 'Adventure/Interesting'],
			    	  validationMessage: "Field is required"
			      },"fitnessRequired": {
			    	  title: "Fitness Required",
			    	  type: "string",
			    	  enum: ['Not Much Fitness','Some Fitness','Reasonable Fitness','Good Fitness','Very Good Fitness'],
			    	  validationMessage: "Field is required"
			      },"ageRange": {
			    	  title: "Age Range",
			    	  type: "string",
			    	  enum: ['Under 18','18-24','25-44','45-64','65+'],
			    	  validationMessage: "Field is required"
			      },"priceRange": {
			    	  title: "Price Range",
			    	  type: "string",
			    	  enum: ['Under £20','£21-£40','£41-£60','£61-£80','£81+'],
			    	  validationMessage: "Field is required"
			      }, "noOfParticipants": {
			    	  title: "Number of Participants",
			    	  type: "integer",
			    	  enum: [1,2,3,4,5],
			    	  default: 1,
			    	  validationMessage: "Field is required"
			      }
			    },
			    required: ["name","company","website","address","postcode","price","activityType","fitnessRequired","ageRange","priceRange","noOfParticipants","postcode","rangeInMiles"]
			};
	 
	 $scope.form = ["name","company","website","address","postcode","price","activityType",
	                "fitnessRequired","ageRange","priceRange",
	                {
	 	            	   "key": "noOfParticipants",
	 	            	   "type": "radios-inline",
	 	            	   "titleMap": [{
	 	            	            	  "value": 1,
	 	            	            	  "name": "1"
	 	            	              }, {
	 	            	            	  "value": 2,
	 	            	            	  "name": "2"
	 	            	              }, {
	 	            	            	  "value": 3,
	 	            	            	  "name": "3"
	 	            	              }, {
	 	            	            	  "value": 4,
	 	            	            	  "name": "4"
	 	            	              }, {
	 	            	            	  "value": 5,
	 	            	            	  "name": "5+"
	 	            	              }]
	 	               }
	                ,{
	 	                 type: "submit",
	 	                 title: "Save Activity"
	 	               }];
	 
	 $scope.model = {};
	 
	 var validateUKPostcode = function(postcode) {
			
			var regex = /^[A-Z]{1,2}[0-9]{1,2} ?[0-9][A-Z]{2}$/i;
			return regex.test(postcode);	
	 };
	 
	 $scope.save = function(form) {
		 $scope.activitySaved = false;
		 $scope.$broadcast('schemaFormValidate');
		 if (form.$valid) {
			 
			 if (!validateUKPostcode(form.postcode.$modelValue)) {
					form.postcode.$invalid = true;
					return;
			 }
				
			 if ($scope.isLoggingNew) {
				 adminService.logNew($scope.model).success(function (data, status) {	
					 if (status === 200) {
						 $scope.activitySaved = true;
						 $scope.model = {'noOfParticipants':1};
						 form.$setPristine();
					 }
				});
			} else {
				adminService.updateExisting($scope.model).success(function (data, status) {	
					 if (status === 200) {
						 $scope.activitySaved = true;
						 $scope.model = {'noOfParticipants':1};
						 $scope.isLoggingNew = true;
						 form.$setPristine();
					 }
				});
			}
		 }
	 };
	 
	 $scope.search = function() {
		 searchService.search($scope.activityName).success(function(data, status){
				if (data.length !== 0) {
					$scope.activities = data;
					$scope.resultsFound = true;
					$scope.resultsEmpty = false;
				} else {
					$scope.resultsFound = false;
					$scope.resultsEmpty = true;
				}
			});
	 };
	 
	 $scope.edit = function(activity) {
		 $scope.model = activity;
		 $scope.isLoggingNew = false;
	 };
	 
	 $scope.clear = function(form) {
		 $scope.activitySaved = false;
		 $scope.model = {'noOfParticipants':1};
		 $scope.isLoggingNew = true;
		 form.$setPristine();
	 };
	 
	 $scope.deleteActivity = function(activity) {
		 var r = confirm("Confirm Delete Activity");
		 if (r === true) {
			 adminService.deleteActivity(activity).success(function (data, status) {	
				 if (status === 200) {
					 $scope.search();
				 }
			});
		 }
	 };
}]);