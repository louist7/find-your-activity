controllers.controller('SearchController', ['$scope', 'searchService', '$location', 'loginService', function ($scope, searchService, $location, loginService) {

	$scope.activityName = '';
	$scope.noResultsMessage = 'No matches found';
	
	$scope.isLoggedIn = loginService.localStorage.isLoggedIn;
	
	$scope.viewingSearch = true;
	$scope.viewingRated = false;
	$scope.viewingNear = false;
	$scope.dataIsLoading = false;
	
	$scope.search = function() {
		$scope.dataIsLoading = true;
		
		$scope.viewingSearch = true;
		$scope.viewingRated = false;
		$scope.viewingNear = false;
		
		searchService.search($scope.activityName).success(function(data, status){
			
			if (data.length !== 0) {
				$scope.activities = data;
				$scope.resultsFound = true;
				$scope.resultsEmpty = false;
				
				$scope.setupPagination();
			} else {
				$scope.resultsFound = false;
				$scope.resultsEmpty = true;
			}
			$scope.dataIsLoading = false;
		});
	};
	
	$scope.viewDetails = function(activity) {
		$location.path('/details/' + activity.id);
	};
	
	var processData = function(data) {
		
		data.sort(function(a, b) {
		    return parseFloat(b.averageRating) - parseFloat(a.averageRating);
		});
		
		var topData = [];
		
		for (var i = 0; i < data.length; i++) {
			data[i].rank = i+1;
			
			var averageRating = data[i].averageRating;
			var numOfRatings = data[i].numOfRatings;
			
			data[i].ratingsDisplay = Math.round(averageRating * 100) / 100 + '('+numOfRatings+')';
			
			topData[i] = data[i];
		}
		return topData;
	};

	var processNearMeData = function(data) {
		
		data.sort(function(a, b) {
		    return parseFloat(a.distance) - parseFloat(b.distance);
		});
		
		var topData = [];
		
		for (var i = 0; i < data.length; i++) {
			data[i].rank = i+1;
			
			var distance = data[i].distance;
			var roundedDistance = ((Number(distance)).toFixed(2));
			data[i].distance = roundedDistance;
			
			topData[i] = data[i];
		}
		return topData;
	};
	
	$scope.highestRated = function() {
		$scope.dataIsLoading = true;
		
		$scope.viewingNear = false;
		$scope.viewingSearch = false;
		$scope.viewingRated = true;
		
		searchService.highestRated().success(function(data, status){
			$scope.activities = processData(data);
			$scope.resultsFound = true;
			$scope.resultsEmpty = false;
			
			$scope.setupPagination();
			
			$scope.dataIsLoading = false;
		});
	};
	
	$scope.usersHighestRated = function() {
		$scope.dataIsLoading = true;
		
		$scope.viewingNear = false;
		$scope.viewingSearch = false;
		$scope.viewingRated = true;
		
		searchService.usersHighestRated(loginService.localStorage.user).success(function(data, status){
			if (status === 200) {
				$scope.notUsedCalculator = false;
				$scope.activities = processData(data);
				$scope.resultsFound = true;
				$scope.resultsEmpty = false;
				
				$scope.setupPagination();
			} else {
				$scope.resultsFound = false;
				$scope.resultsEmpty = true;
			}
			$scope.dataIsLoading = false;
		});
	};
	
	$scope.nearActivities = function() {
		$scope.dataIsLoading = true;
		
		$scope.viewingNear = true;
		$scope.viewingSearch = false;
		$scope.viewingRated = false;
		
		searchService.nearActivities(loginService.localStorage.user).success(function(data, status){
			if (status === 200) {
				$scope.notUsedCalculator = false;
				$scope.activities = processNearMeData(data);
				$scope.resultsFound = true;
				$scope.resultsEmpty = false;
				
				$scope.setupPagination();
			} else {
				$scope.resultsFound = false;
				$scope.resultsEmpty = true;
			}
			$scope.dataIsLoading = false;
		});
	};
	
	$scope.setupPagination = function() {
		
		$scope.currentPage = 1;
		$scope.numPerPage = 10;
		$scope.maxSize = 5;
		
		$scope.$watch("currentPage + numPerPage", function() {
		    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
		    var end = begin + $scope.numPerPage;

		    $scope.filteredActivities = $scope.activities.slice(begin, end);
		});
	};
}]);
