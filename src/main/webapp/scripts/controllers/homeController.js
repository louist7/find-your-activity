controllers.controller('HomeController', ['$scope', 'homeService', 'loginService', '$location', '$anchorScroll', '$route', 'featureFlags',
                                          function ($scope, homeService, loginService, $location, $anchorScroll, $route, featureFlags) {

	$scope.pageTitle = 'Activity Calculator';
	$scope.noResultsMessage = 'No matches found';
	$scope.userLoggedIn = loginService.localStorage.isLoggedIn;
	$scope.dataIsLoading = false;
	
	$scope.loadUserData = function() {
		
		if (loginService.localStorage.isLoggedIn) {
			var username = loginService.localStorage.user.username;
			$scope.logInMessage = 'You are Logged In as: ' + username;
			homeService.retrieveUserData(username).success(function(data, status) {
				$scope.model = data;
			});
		} else {
			$scope.logInMessage = 'You are Not Currently Logged In';
			$scope.model = {};
		}
	};
	
	var processData = function(data) {
		
		data.sort(function(a, b) {
		    return parseFloat(b.coefficient) - parseFloat(a.coefficient);
		});
		
		var topData = [];
		
		for (var i = 0; i < data.length; i++) {
			
			data[i].rank = i+1;
			
			var coefficient = data[i].coefficient;
			var rating = ((Number(coefficient)).toFixed(2)) * 100;
			data[i].coefficient = (rating.toFixed()).toString() + '%';
			
			topData[i] = data[i];
		}
		return topData;
	};
	
	var validateUKPostcode = function(postcode) {
		
		var regex = /^[A-Z]{1,2}[0-9]{1,2} ?[0-9][A-Z]{2}$/i;
		return regex.test(postcode);
	};
	
	$scope.calculateResults = function(form) {
		
		$scope.$broadcast('schemaFormValidate');
		if (form.$valid) {
			
			if (!validateUKPostcode(form.postcode.$modelValue)) {
				form.postcode.$invalid = true;
				return;
			}
			
			$scope.dataIsLoading = true;
			if (loginService.localStorage.isLoggedIn) {
				loginService.localStorage.user = $scope.model;
				homeService.calculateResultsForUser($scope.model, loginService.localStorage.user.username).success(function (data, status) {
					if (status === 204) {
						$scope.resultsFound = false;
						$scope.resultsEmpty = true;
					} else {
					
						if (data.length !== 0) {
							$scope.resultsFound = true;
							$scope.resultsEmpty = false;
						
							$scope.activities = processData(data);
							
							$scope.setupPagination();
						
							var old = $location.hash();
							$location.hash('results');
							$anchorScroll();
							$location.hash(old);
						} else {
							$scope.resultsFound = false;
							$scope.resultsEmpty = true;
						}
					}
					$scope.dataIsLoading = false;
				});
			} else {
				homeService.calculateResultsForGuest($scope.model).success(function (data, status) {
					if (data.length !== 0) {
						$scope.resultsFound = true;
						$scope.resultsEmpty = false;
						
						$scope.activities = processData(data);
						
						$scope.setupPagination();
						
						var old = $location.hash();
						$location.hash('results');
						$anchorScroll();
						$location.hash(old);
					} else {
						$scope.resultsFound = false;
						$scope.resultsEmpty = true;
					}
					$scope.dataIsLoading = false;
				});
			}
		 }
	};
	
	$scope.setupPagination = function() {
		
		$scope.currentPage = 1;
		$scope.numPerPage = 10;
		$scope.maxSize = 5;
		
		$scope.$watch("currentPage + numPerPage", function() {
		    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
		    var end = begin + $scope.numPerPage;

		    $scope.filteredActivities = $scope.activities.slice(begin, end);
		});
	};
	
	$scope.viewDetails = function(activity) {
		$location.path('/details/' + activity.id);
	};
	
	$scope.navToLogin = function() {
		$location.path('/user');
	}
	
	$scope.logout = function() {
		 loginService.localStorage.isLoggedIn = false;
		 loginService.localStorage.user = {};
		 $scope.userLoggedIn = loginService.localStorage.isLoggedIn;
		 $scope.logInMessage = 'You are Not Currently Logged In';
		 $scope.model = {};
		 $scope.resultsFound = false;
		 
		 var flagData = [{"key": "admin", "active": false}]
	     featureFlags.set(flagData);
		 
		 $route.reload();
	};
 
	$scope.schema = {
		    type: "object",
		    properties: {
		    	"interests": {
		    		title: "Interests",
			    	type: "string", 
			    	enum: ['Relaxing', 'Indoor Sports', 'Team Sports', 'Outdoor Sports', 'Adventure/Interesting'],
			    	validationMessage: "Missing Interests"
		    	},
		    	"fitnessLevel": {
		    		title: "Fitness Level",
			    	type: "string", 
			    	enum: ['Not Much Fitness','Some Fitness','Reasonable Fitness','Good Fitness','Very Good Fitness'],
		    		validationMessage: "Missing Fitness Level"
		    	},
		    	"ageRange": {
		    		title: "Age",
			    	type: "string", 
			    	enum: ['Under 18','18-24','25-44','45-64','65+'],
		    		validationMessage: "Missing Age Range"
		    	},
			    "budgetRange": {
			    	title: "Budget",
			    	type: "string",
			    	enum: ['Under £20','£21-£40','£41-£60','£61-£80','£81+'],
			    	validationMessage: "Missing Budget"
			    },
			    "noOfParticipants": {
			    	title: "Number of Participants",
			    	type: "integer",
			    	enum: [1,2,3,4,5],
			    	default: 1,
			    	validationMessage: "Missing Participants"
			    },
			    "postcode": {
			    	title: "Postcode",
			    	type: "string",
			    	validationMessage: "Not a Valid UK Postcode"
			    },
			    "rangeInMiles": {
			    	title: "Within Range",
			    	type: "integer",
			    	enum: [1,5,10,20,50],
			    	validationMessage: "Missing Distance Range"
			    }
		    },
		    required: ["interests","fitnessLevel","ageRange","budgetRange","noOfParticipants","postcode","rangeInMiles"]
	};

	$scope.form = [
	               "interests",
	               "fitnessLevel",
 	               "ageRange",
 	               "budgetRange",
 	               {
 	            	   "key": "noOfParticipants",
 	            	   "type": "radios-inline",
 	            	   "titleMap": [{
 	            	            	  "value": 1,
 	            	            	  "name": "1"
 	            	              }, {
 	            	            	  "value": 2,
 	            	            	  "name": "2"
 	            	              }, {
 	            	            	  "value": 3,
 	            	            	  "name": "3"
 	            	              }, {
 	            	            	  "value": 4,
 	            	            	  "name": "4"
 	            	              }, {
 	            	            	  "value": 5,
 	            	            	  "name": "5+"
 	            	              }]
 	               },
 	               { 
 	            	   "type": "section",
 	            	   "htmlClass": "row",
 	            	   "items": [ {
 	            	            	 "type": "section",
 	            	            	 "htmlClass": "col-xs-6",
 	            	            	 "items": ["postcode"]
 	            	             },
 	            	             {
 	            	            	 "type": "section",
 	            	            	 "htmlClass": "col-xs-6",
 	            	            	 "items": [ {
 	            	            		 "key" : "rangeInMiles",
 	            	            		 "type": "select",
 	            	            		 "titleMap" : [ {
 	            	            			 "value" : 1,
 	            	            			 "name" : "1 Mile"
 	            	            		 }, {
 	            	            			 "value" : 5,
 	            	            			 "name" : "5 Miles"
 	            	            		 }, {
 	            	            			 "value" : 10,
 	            	            			 "name" : "10 Miles"
 	            	            		 }, {
 	            	            			 "value" : 20,
 	            	            			 "name" : "20 Miles"
 	            	            		 }, {
 	            	            			 "value" : 50,
 	            	            			 "name" : "50 Miles"
 	            	            		 } ]
 	            	            	 } ]
 	            	             } ]
 	              },
 	             {
 	                 type: "submit",
 	                 title: "Find My Activities"
 	               }
 	             ];
	
}]);