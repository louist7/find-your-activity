controllers.controller('LoginController', ['$scope', 'loginService', 'featureFlags', function ($scope, loginService, featureFlags) {
	 
	 $scope.isLoggedIn = loginService.localStorage.isLoggedIn;
	 
	 $scope.dataIsLoading = false;
	 
	 $scope.userDistance = '1';
	 
	 var checkLogin = function() {
		 if (loginService.localStorage.isLoggedIn) {
			 $scope.loggedInMessage = 'You are Logged In as: ' + loginService.localStorage.user.username;
			 
			 $scope.updateDetailsModel = {
					 "email" : loginService.localStorage.user.email,
					 "phone" : loginService.localStorage.user.phone,
					 "visibleProfile" : loginService.localStorage.user.visibleProfile
			 };
		 }
	 }
	 checkLogin();
	 
	 $scope.emailContent = '?subject=We Ranked Similarly on Find Your Activity!&body=Hi there, we had a high similarity ranking on Find Your Activity.';
	 
	 $scope.form = [
		               "*"
	               ];

	 $scope.signupSchema = {
			    type: "object",
			    properties: {
			      "username": {
			    	  title: "Username",
			    	  type: "string",
			    	  required: true,
			    	  minLength: 2,
			    	  validationMessage: "Username must be at least 2 characters long"
			    	    
			      },
			      "password": {
			    	  title: "Password",
			    	  type: "string",
			    	  required: true,
			    	  minLength: 2,
			    	  validationMessage: "Password must be at least 2 characters long"
			      },
			      "confirmPassword": {
			    	  title: "Confirm Password",
			    	  type: "string",
			    	  required: true,
			    	  minLength: 2,
			    	  validationMessage: "Passwords Must Match"
			      }
			    }
			};

	 $scope.signupModel = {};
	 
	 var setLoginVariables = function(data) {
		 loginService.localStorage.isLoggedIn = true;
		 loginService.localStorage.user = data;
		 $scope.isLoggedIn = loginService.localStorage.isLoggedIn;
		 $scope.loggedInMessage = 'You are Logged In as: ' + loginService.localStorage.user.username;
		 $scope.profileUpdated = false;
		 $scope.usersFound = false;
		 $scope.notUsedCalculator = false; 
		 $scope.updateDetailsModel = {
				 "email" : loginService.localStorage.user.email,
				 "phone" : loginService.localStorage.user.phone,
				 "visibleProfile" : loginService.localStorage.user.visibleProfile
		 };
		 
		 if (data.admin) {
			 var flagData = [{"key": "admin", "active": true}]
		     featureFlags.set(flagData);
		 }
		 
		 $scope.loggedInMessage = 'You are Logged In as: ' + loginService.localStorage.user.username;
	 };
	 
	 $scope.login = function(username, password) {
		 
		 var loginForm = {
				 username : username,
				 password : password};
		 
		 loginService.login(loginForm).success(function (data, status) {
			 if (status === 200) {
				 setLoginVariables(data); 
			 } else if (status === 204) {
				 $scope.incorrectLoginMessage = 'Username or Password is Incorrect';
			 }
		 });
	 };
	 
	 $scope.signup = function(username, password, confirmPassword) {
		 
		 var signupForm = {
				 username : username,
				 password : password};
		 
		 if (password !== confirmPassword) {
			 $scope.incorrectSignupMessage = 'Passwords do not match';
		 } else {
			 loginService.signup(signupForm).success(function (data, status) { 
				 if (status === 200) {	 
					 setLoginVariables(data);
				 } else if (status === 204) {
					 $scope.incorrectSignupMessage = 'Username already exists';
				 }
			 });
		 }
	 };
	 
	 $scope.logout = function() {
		 loginService.localStorage.isLoggedIn = false;
		 loginService.localStorage.user = {};
		 $scope.isLoggedIn = loginService.localStorage.isLoggedIn;
		 $scope.incorrectLoginMessage='';
		 $scope.incorrectSignupMessage='';
		 
		 $scope.username = '';
		 $scope.password = '';
		 
		 $scope.signupUsername = '';
		 $scope.signupPassword = '';
		 $scope.signupConfirmPassword = '';
		 
		 var flagData = [{"key": "admin", "active": false}]
	     featureFlags.set(flagData);
	 };
	 
	 $scope.updateDetailsSchema = {
			 type: "object",
			    properties: {
			      "email": {
			    	  title: "Email Address",
			    	  type: "string"
			      },
			      "phone": {
			    	  title: "Phone Number",
			    	  type: "string"
			      },
			      "visibleProfile": {
			    	  title: "Profile Is Visible to Other Users",
			    	  type: "boolean"
			      } 
			    }
	 };
	 
	 var processData = function(data) {

		 data.sort(function(a, b) {    
			 return parseFloat(b.coefficient) - parseFloat(a.coefficient);
		 });
		 
		 var topData = [];
		 
		 for (var i = 0; i < data.length; i++) {
			 
			 data[i].rank = i+1;
			 
			 var coefficient = data[i].coefficient;
			 var rating = ((Number(coefficient)).toFixed(2)) * 100;	
			 data[i].coefficient = (rating.toFixed()).toString() + '%';
			 
			 topData[i] = data[i];
		 }
		 return data;
	 };
		
	 $scope.findSimilarUsers = function() {
		 var user = loginService.localStorage.user;
		 
		 if (!user.interests) {
			 $scope.notUsedCalculator = true;
			 $scope.notUsedCalculatorMessage = 'You Must First Use The Activity Calculator';
		 } else {
			 $scope.dataIsLoading = true;
			 $scope.notUsedCalculator = false;
			 loginService.findSimilarUsers(user, $scope.userDistance).success(function (data, status) {
				 if (status === 204) {
					 $scope.notUsedCalculator = true;
					 $scope.notUsedCalculatorMessage = 'No Matches Found';
				 } else {
					 $scope.usersFound = true;
					 $scope.similarUsers = processData(data);
				 
					 $scope.setupPagination();
				 }
				 $scope.dataIsLoading = false;
			 }); 
		 }
	 };
	 
	 $scope.updateProfile = function() {
		 loginService.updateProfile(loginService.localStorage.user.username, $scope.updateDetailsModel).success(function (data, status) {	
			 loginService.localStorage.user = data;
			 $scope.profileUpdated = true;
			 $scope.profileUpdatedMessage = 'Profile Details Saved';
		 });
	 };
	 
	 $scope.setupPagination = function() {
			
			$scope.currentPage = 1;
			$scope.numPerPage = 10;
			$scope.maxSize = 5;
			
			$scope.$watch("currentPage + numPerPage", function() {
			    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
			    var end = begin + $scope.numPerPage;

			    $scope.filteredSimilarUsers = $scope.similarUsers.slice(begin, end);
			});
		};
}]);