alter table users
add column interests varchar(20),
add column fitness_level varchar(20),
add column age_range varchar(20),
add column no_of_participants integer,
add column postcode varchar(20),
add column range_in_miles integer,
add column budget_range varchar(20);