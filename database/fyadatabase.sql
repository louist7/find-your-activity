-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: findyouractivity
-- ------------------------------------------------------
-- Server version	5.6.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `activity_type` varchar(100) DEFAULT NULL,
  `fitness_required` varchar(20) DEFAULT NULL,
  `age_range` varchar(20) DEFAULT NULL,
  `no_of_participants` int(11) DEFAULT NULL,
  `postcode` varchar(20) DEFAULT NULL,
  `price_range` varchar(20) DEFAULT NULL,
  `company` varchar(50) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `website` varchar(200) DEFAULT NULL,
  `price` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
INSERT INTO `activities` VALUES (1,'Badminton','Indoor Sports','Some Fitness','Under 18',1,'LE113HE','Under £20','No Strings Badminton','Loughborough Leisure Centre, Browns Lane, Loughborough, Leicestershire','http://www.nostringsbadminton.co.uk/','£15'),(2,'Abseiling','Adventure/Interesting','Very Good Fitness','25-44',1,'WR66NH','£41-£60','Top Barn','Holt Heath, Worcester','http://www.yumping.co.uk/abseiling/top-barn-activity-centre-abseiling--e19663003','£50'),(3,'Martial Arts','Adventure/Interesting','Good Fitness','Under 18',1,'LE113DU','Under £20','East Midlands Martial Arts','Loughborough Martial Arts Education Centre, 1a Granby Street, Loughborough','http://emkmartialarts.co.uk/','Free'),(4,'Zumba','Indoor Sports','Not Much Fitness','25-44',1,'DE76GX','Under £20','It\'s Zumba Time','West Hallam Community Centre, Jubilee Ct, West Hallam, Ilkeston, Derbyshire','http://www.itszumbatime.co.uk/news.htm','£4'),(5,'Rock Climbing','Adventure/Interesting','Very Good Fitness','25-44',1,'NG76AT','Under £20','Nottingham Climbing Centre','The Old Pool, 212 Noel Street, New Basford, Nottingham','http://www.nottingham-climbing.co.uk/','£8'),(6,'Bubble Football','Team Sports','Reasonable Fitness','18-24',1,'NG190EE','£81+','East Midlands Bubble Football','East Midlands Bubble Football, Forest Town Arena, Clipstone Road West, Forest Town','http://eastmidlandsbubblefootball.co.uk/what-is-bubble-football/3607415','Varies by location'),(7,'Off Road Horse Riding','Adventure/Interesting','Good Fitness','18-24',1,'NG209RH','£21-£40','Derbyshire Pony Trekking','2 Old Hall, Scarcliffe Lanes, Upper Langwith,  Mansfield, Nottinghamshire','http://www.emasarchery.co.uk/','Varies'),(8,'Go Karting','Adventure/Interesting','Some Fitness','25-44',1,'NG172NB','£21-£40','The Kurburgring','The Kurburgring, Units 4 and 5, Fulwood Road North, Huthwaite, Nottinghamshire','http://www.kurburgring.co.uk/','£25'),(9,'Yoga','Indoor Sports','Not Much Fitness','45-64',1,'NG51DG','£41-£60','The Yoga Place','D18 Hartley House, Haydn Road, Sherwood Nottingham, Nottinghamshire','http://www.yogastudiosaround.co.uk/business/the-yoga-place','Varies'),(10,'Watersports','Outdoor Sports','Good Fitness','18-24',1,'WS87NL','£21-£40','Chasewater Wake & Ski School','Chasewater Country Park Pool Road','http://chasewaterski.co.uk/adult-lessons.php','£22'),(11,'Watersports','Outdoor Sports','Good Fitness','Under 18',1,'WS87NL','Under £20','Chasewater Wake & Ski School','Chasewater Country Park Pool Road','http://chasewaterski.co.uk/kids-lessons.php','£10'),(12,'Indoor Mini Golf','Relaxing','Not Much Fitness','18-24',1,'NG14DB','Under £20','The Lost City','The Corner House, Nottingham','http://www.lostcityadventuregolf.com/','£7.50'),(13,'Golf','Relaxing','Some Fitness','65+',1,'DE215BL','£21-£40','Horsley Lodge Golf Club','Smalley Mill Road, Horsley, Derbyshire','http://www.horsleylodge.co.uk/','£25 - £35'),(14,'Swimming','Indoor Sports','Some Fitness','45-64',1,'S402ND','Under £20','Various','Boythorpe Road, Chesterfield','http://www.swimming.org/EastMidland/','Free'),(15,'Bowling','Indoor Sports','Not Much Fitness','18-24',1,'NG72UW','Under £20','Ten Pin Bowlin','Clifton Boulevard, Redfield Way Nottingham','https://www.tenpin.co.uk/','£6.99'),(17,'Bowling','Indoor Sports','Not Much Fitness','65+',1,'NG72UW','Under £20','Ten Pin Bowling','Clifton Boulevard, Redfield Way Nottingham','https://www.tenpin.co.uk','£6.99'),(18,'Fishing','Outdoor Sports','Some Fitness','45-64',1,'NG165JQ','£21-£40','Midland Angling Society','27 Rutland Road, Notts, Nottinghamshire','http://www.midlandanglingsociety.com/p/midlands-as.html','£32.50'),(19,'Off Road Cycling','Outdoor Sports','Very Good Fitness','25-44',1,'Ng219JL','Under £20','Sherwood Pines','Edwinstowe, Mansfield','http://www.forestry.gov.uk/forestry/infd-8z4lt8','Free'),(20,'Volleyball','Team Sports','Good Fitness','Under 18',1,'NG82AE','Under £20','Wollaton Park Taster Sessions','Wollaton Park, Nottingham','http://www.eastmidlandsvb.com/county-news/nottinghamshire-volleyball-news/422-nottingham-volleyball-wollaton-park-taster-sessions.html','Free'),(21,'Rowing','Team Sports','Very Good Fitness','18-24',1,'NG25FA','Under £20','Nottingham Rowing Club','Middle-of-Three, Trentside North, West Bridgford','http://www.jpaulwilliamson.co.uk/','Free'),(22,'Skiing','Outdoor Sports','Good Fitness','18-24',1,'DE118LP','£61-£80','Swadlincote Ski & Snowboard Centre','Sir Herbert Wragg Way, Swadlincote, Derbyshire','http://www.jnlswadlincote.co.uk/skiing/lessons/','£68'),(23,'Archery','Outdoor Sports','Reasonable Fitness','Under 18',1,'DE217PH','Under £20','Anchor Bowmen Archery Club','Asterdale Grounds, Borrowash, Derby','http://www.anchorbowmen.co.uk/','£10');
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratings`
--

DROP TABLE IF EXISTS `ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `activity_id` bigint(20) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ratings`
--

LOCK TABLES `ratings` WRITE;
/*!40000 ALTER TABLE `ratings` DISABLE KEYS */;
INSERT INTO `ratings` VALUES (1,5,4,4),(3,5,5,4),(6,5,1,4),(7,2,4,3),(8,2,5,1),(9,2,2,2),(10,2,1,1),(11,3,2,3),(12,7,18,5),(13,7,17,3),(14,7,19,4),(15,7,13,2),(16,7,9,1),(17,7,4,2),(18,7,5,5),(19,7,15,3),(20,7,8,5),(21,7,1,1),(22,7,21,2),(23,5,11,4),(24,5,19,4),(25,5,3,2),(26,5,7,1),(27,5,10,5),(28,5,20,1),(29,5,21,3),(30,5,8,2),(31,5,18,4),(32,5,15,2),(33,5,6,2),(34,5,16,2),(35,5,14,2),(36,5,22,5),(37,5,12,2),(38,3,9,5),(39,3,13,3),(40,3,18,4),(41,3,4,5),(42,3,22,1),(43,3,10,1),(44,3,11,1),(45,3,19,1),(46,3,17,4),(47,3,8,1),(48,3,12,5),(49,3,16,5),(50,2,9,3),(51,9,6,5),(52,9,22,4),(53,9,2,2),(54,9,10,4),(55,9,21,2),(56,9,19,2),(57,9,11,5),(58,9,17,1),(59,9,12,3),(60,8,18,2),(61,8,4,4),(62,8,6,1),(63,8,9,5),(64,8,13,1),(65,8,12,5),(66,8,17,5),(67,6,16,4),(68,6,22,2),(69,6,3,2),(70,6,1,5),(71,6,15,4),(72,6,9,3),(73,6,5,3),(74,6,12,4),(75,6,11,5),(76,6,10,5),(77,10,12,5),(78,10,13,3),(79,10,9,2),(80,10,6,1),(81,10,14,5),(82,10,7,3),(83,10,20,1),(84,11,8,5),(85,11,7,5),(86,11,16,1),(87,11,18,1),(88,11,20,5),(89,11,11,3),(90,11,12,4),(91,11,10,4),(92,11,9,4),(93,11,19,1),(94,11,5,3),(95,11,15,5),(96,11,14,2),(97,12,5,5),(98,12,19,5),(99,12,11,3),(100,12,20,1),(101,12,10,3),(102,12,7,1),(103,12,3,2),(104,13,6,5),(105,13,22,1),(106,13,9,4),(107,13,19,4),(108,13,1,3),(109,13,18,4),(110,13,5,4),(111,13,4,5),(112,13,14,2),(113,13,17,2),(114,13,8,3),(115,13,2,2),(116,13,3,1),(117,13,21,2),(118,13,20,4),(119,13,16,2),(120,13,11,3),(121,13,7,2),(122,13,13,1),(123,14,15,2),(124,14,16,1),(125,14,8,5),(126,14,18,1),(127,14,4,2),(128,14,10,4),(129,14,12,2),(130,14,5,5),(131,14,9,1),(132,14,14,3),(133,14,22,5),(134,14,13,3),(135,14,2,4),(136,14,3,2),(137,14,21,2),(138,14,19,4),(139,14,7,1),(140,14,6,3),(141,1,11,3),(142,16,17,5),(143,16,3,2),(144,16,1,4),(145,16,11,5),(146,16,21,2),(147,16,19,3),(148,16,20,5),(149,16,16,4),(150,16,8,2),(151,15,15,5),(152,15,12,4),(153,15,16,4),(154,15,17,4),(155,15,1,3),(156,15,21,1),(157,1,23,1),(158,1,4,1),(159,5,23,4),(160,5,17,2),(161,11,23,1),(162,2,23,1),(163,16,12,2),(164,16,10,1),(165,16,5,2),(166,2,17,1),(167,2,15,2);
/*!40000 ALTER TABLE `ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `interests` varchar(100) DEFAULT NULL,
  `fitness_level` varchar(20) DEFAULT NULL,
  `age_range` varchar(20) DEFAULT NULL,
  `no_of_participants` int(11) DEFAULT NULL,
  `postcode` varchar(20) DEFAULT NULL,
  `range_in_miles` int(11) DEFAULT NULL,
  `budget_range` varchar(20) DEFAULT NULL,
  `admin` tinyint(1) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `visible_profile` tinyint(1) DEFAULT NULL,
  `phone` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','password','Adventure/Interesting','Very Good Fitness','25-44',2,'NG118qp',50,'Under £20',1,'admin@fya.com',0,''),(2,'greg','pass','Relaxing','Some Fitness','18-24',2,'NG118qp',50,'£21-£40',0,'gw@hotmail.com',1,''),(3,'sandra','test','Indoor Sports','Not Much Fitness','45-64',1,'LE113HE',50,'£41-£60',0,'sandra@mail.com',1,''),(5,'louis','pass','Outdoor Sports','Good Fitness','18-24',2,'LE113HE',50,'Under £20',0,'louist7@gmail.com',1,'07587184035'),(6,'Paul','Hello123','Outdoor Sports','Some Fitness','45-64',1,'LE113HE',20,'£41-£60',0,'paul@mail.com',1,''),(7,'John','Hello','Outdoor Sports','Reasonable Fitness','45-64',1,'LE11EA',50,'£21-£40',0,'john2@gmail.com',1,''),(8,'theresa','hello123','Relaxing','Not Much Fitness','65+',2,'NG11AA',50,'£41-£60',0,'theresa@mail.com',1,''),(9,'Tim Jones','jonesy','Team Sports','Very Good Fitness','25-44',1,'NG51AA',20,'£81+',0,'tim.jones@gmail.com',1,''),(10,'Nicole','kitty','Relaxing','Some Fitness','18-24',1,'NG118QP',50,'£21-£40',0,'ns@hotmail.com',1,''),(11,'laura_t','password1','Adventure/Interesting','Reasonable Fitness','25-44',2,'S427HH',50,'£41-£60',0,'laura_t16@mail.com',1,''),(12,'tom','hello','Outdoor Sports','Good Fitness','25-44',3,'NG14FT',10,'Under £20',0,'tomm@hotmail.co.uk',1,''),(13,'michael','owen','Team Sports','Very Good Fitness','45-64',1,'LE11AA',50,'£81+',0,'michael02@gmail.com',1,''),(14,'Eric_Johnson','ericj','Adventure/Interesting','Some Fitness','45-64',3,'DE221BT',20,'£41-£60',0,'eric_j@hotmail.com',1,''),(15,'nsummers','tinkerbelle','Indoor Sports','Not Much Fitness','18-24',1,'NN172EE',50,'Under £20',0,'nsummers@mail.com',1,''),(16,'Amelia','pass','Team Sports','Reasonable Fitness','18-24',3,'NG118qp',50,'Under £20',0,'amelia@gmail.com',1,''),(17,'mark','marko',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'',1,NULL),(18,'marcus','hello',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'',1,''),(19,'dave','mickeymouse',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'',1,'');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-03 19:50:33
