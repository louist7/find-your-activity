CREATE TABLE ratings (
	id bigint auto_increment,
    user_id bigint,
    activity_id bigint,
    rating int,
    primary key(id)
);