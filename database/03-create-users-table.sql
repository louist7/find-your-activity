CREATE TABLE users (
	id bigint auto_increment,
    username VARCHAR(255),
    password VARCHAR(255),
    primary key(id)
);