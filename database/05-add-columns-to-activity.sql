alter table activities
add column activity_type varchar(20),
add column fitness_required varchar(20),
add column age_range varchar(20),
add column no_of_participants integer,
add column postcode varchar(20),
add column price_range varchar(20),
add column company varchar(50),
add column address varchar(200),
add column price varchar(50),
add column website varchar(50);